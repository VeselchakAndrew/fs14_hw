package HW005;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HumanTest {
Human testedHuman = new Human("John", "Dow", 1980);
    @Test
    void testToString() {
        String expectedString = "Human {name = John, surname = Dow, year = 1980, iq = 0, schedule = null}";
        assertEquals(expectedString, testedHuman.toString());
    }
}