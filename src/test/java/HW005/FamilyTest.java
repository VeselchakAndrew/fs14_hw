package HW005;

import org.junit.jupiter.api.*;

import static org.junit.jupiter.api.Assertions.*;

class FamilyTest {
    private static final Human testedFather = new Human("Mike", "Dow", 1955);
    private static final Human testedMother = new Human("Joan", "Dow", 1960);
    private static final Human child = new Human("John", "Dow", 1980);
    private static final Family testedFamily = new Family(testedMother, testedFather);

    @BeforeEach
    void init() {
        for (int i = 0; i < 4; i++) {
            testedFamily.addChild(new Human("Name" + i, "Surname" + i, 1990 + i));
        }
        testedFamily.addChild(child);
    }

    @Test
    void when_Added5Children_then_ChildrenArraySizeMust5() {
        testedFamily.addChild(child);
        assertEquals(6, testedFamily.getChildren().length);
    }

    @Test
    void when_removeChildByIndex_then_ChildrenArraySizeMustBeDecreased() {
        testedFamily.deleteChildByIndex(1);
        assertEquals(4, testedFamily.getChildren().length);
    }

    @Test
    void when_removeChildByWrongIndex_then_ChildrenArraySizeMustBeUnchanged() {
        testedFamily.deleteChildByIndex(10);
        assertEquals(5, testedFamily.getChildren().length);
    }

    @Test
    void when_removeChild_then_ChildrenArraySizeMustBeDecreased() {
        testedFamily.deleteChild(child);
        int index = -1;
        for (int i = 0; i < testedFamily.getChildren().length; i++) {
            if (testedFamily.getChildren()[i].equals(child)) {
                index = i;
                break;
            }
        }
        assertEquals(4, testedFamily.getChildren().length);
        assertEquals(-1, index);
    }

    @Test
    void when_removeChildThatNotExist_then_ChildrenArraySizeMustBeUnchanged() {
        Human testedChild = new Human("John", "Connor", 1985);
        testedFamily.deleteChild(testedChild);
        assertEquals(5, testedFamily.getChildren().length);
    }

    @Test
    void countFamily() {
        assertEquals(7, testedFamily.countFamily());
    }

    @Test
    void testToString() {
        testedFamily.setChildren(new Human[0]);
        testedFamily.addChild(child);
        String expectedString = "Family: Dow\n" +
                "Father: Human {name = Mike, surname = Dow, year = 1955, iq = 0, schedule = null}\n" +
                "Mother: Human {name = Joan, surname = Dow, year = 1960, iq = 0, schedule = null}\n" +
                "Children: [Human {name = John, surname = Dow, year = 1980, iq = 0, schedule = null}]\n" +
                "Pet: The family has no pet";
        assertEquals(expectedString, testedFamily.toString());

    }

    @AfterEach
    void purgeTestedFamilyObject() {
        testedFamily.setChildren(new Human[0]);
    }
}
