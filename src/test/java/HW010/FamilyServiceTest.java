package HW010;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import java.text.ParseException;

import static org.junit.jupiter.api.Assertions.*;

class FamilyServiceTest {

    private final FamilyService familyService = new FamilyService();
    private final Man man = new Man("John", "Dow", "30/10/1975");
    private final Woman woman = new Woman("Jane", "Dow", "04/04/1980");
    private final Human child = new Human("Miranda", "Dow", "01/10/1997");
    private final Family family = new Family(woman, man);

    private final Man manEmpty = new Man();
    private final Woman womanEmpty = new Woman();
    private final Family familyEmpty = new Family(womanEmpty, manEmpty);

    private final Pet fish = new Fish(Species.FISH, "Goldy");

    FamilyServiceTest() throws ParseException {
    }

    @BeforeEach
    void init() throws ParseException {
        for (int i = 0; i < 10; i++) {
            Man man = new Man("FatherName" + i, "Surname" + i, "10" + i + "/" + "01" + i + "/" + "1980" + i);
            Woman woman = new Woman("MotherName" + i, "Surname" + i, "10" + i + "/" + "01" + i + "/" + "1980" + i);
            Human child = new Human("ChildName" + i, "Surname" + i, "10" + i + "/" + "01" + i + "/" + "2000" + i);
            Family family = new Family(woman, man);
            family.addChild(child);
            familyService.saveFamily(family);
        }

        Human child1 = new Human("Bill", "Dow", "01/12/2001");
        Human child2 = new Human("Bob", "Dow", "06/05/2003");
        Human child3 = new Human("Jane", "Dow", "12/09/2005");
        Human child4 = new Human("Jil", "Dow", "25/07/2007");
        family.addChild(child1);
        family.addChild(child2);
        family.addChild(child3);
        family.addChild(child4);
        familyService.saveFamily(family);
    }

    @Test
    void should_returnAllFamilies_when_FamiliesNotEmpty() {
        assertEquals(11, familyService.getAllFamilies().size());
    }

    @Test
    void should_returnFamiliesBiggerThanNumber() {
        int numberOfMemberOfFamily = 4;
        assertEquals(1, familyService.getFamiliesBiggerThan(numberOfMemberOfFamily).size());

    }

    @Test
    void should_ReturnFamiliesLessThanNumber() {
        int numberOfMemberOfFamily = 4;
        assertEquals(10, familyService.getFamiliesLessThan(numberOfMemberOfFamily).size());
    }

    @Test
    void countFamiliesWithMemberNumber() {
        family.addChild(child);
        familyService.saveFamily(family);
        int numberOfMemberOfFamily = 7;
        assertEquals(1, familyService.countFamiliesWithMemberNumber(numberOfMemberOfFamily).size());

    }

    @Test
    void should_IncreaseFamilySize_when_AddedNewFamily() {

        familyService.saveFamily(familyEmpty);
        assertEquals(12, familyService.getAllFamilies().size());
    }

    @Test
    void should_FamilySizeNotChanged_when_FamilyUpdated() throws ParseException {
        Human child = new Human("Bob", "Dow", "24/10/2010");
        family.addChild(child);
        familyService.saveFamily(family);
        assertEquals(11, familyService.getAllFamilies().size());
    }

    @Test
    void should_DecreaseFamiliesSize_when_deletedIndexIsExist() {
        familyService.deleteFamilyByIndex(0);
        assertEquals(10, familyService.getAllFamilies().size());
    }

    @Test
    void should_ReturnTrue_when_deletedIndexIsExist() {
        assertTrue(familyService.deleteFamilyByIndex(0));
    }

    @Test
    void should_FamiliesSizeNotChanged_when_deletedIndexIsNotExist() {
        familyService.deleteFamilyByIndex(15);
        assertEquals(11, familyService.getAllFamilies().size());
    }

    @Test
    void should_ReturnFalse_when_deletedIndexIsNotExist() {
        assertFalse(familyService.deleteFamilyByIndex(15));
    }

    @Test
    void should_DecreaseFamiliesSize_when_deletedFamilyIsExist() {
        familyService.deleteFamily(family);
        assertEquals(10, familyService.getAllFamilies().size());
    }

    @Test
    void should_ReturnTrue_when_deletedFamilyIsExist() {
        familyService.saveFamily(family);
        assertTrue(familyService.deleteFamily(family));
    }

    @Test
    void should_FamiliesSizeNotChanged_when_deletedFamilyIsExist() {
        familyService.deleteFamily(family);
        assertEquals(10, familyService.getAllFamilies().size());
    }

    @Test
    void should_ReturnTrue_when_deletedFamilyIsNotExist() {
        familyService.deleteAllFamilies();
        assertFalse(familyService.deleteFamily(family));
    }

    @Test
    void should_AddNewBornChild_when_FamilyIsExist() throws ParseException {
        familyService.bornChild(family);
        assertEquals(5, familyService.getFamilyById(10).getChildren().size());
    }

    @Test
    void should_ThrowTheError_when_FamilyForNewBornIsNotExist() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            familyService.bornChild(familyEmpty);
        });

        String expectedMessage = "Сначала надо создать семью!";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }


    @Test
    void should_AddAdoptChild_when_FamilyIsExist() {
        Human adoptedChild = new Human();
        familyService.adoptChild(family, adoptedChild);
        assertEquals(5, familyService.getFamilyById(10).getChildren().size());
    }

    @Test
    void should_ThrowTheError_when_FamilyForAdoptIsNotExist() {

        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            familyService.adoptChild(familyEmpty, child);
        });

        String expectedMessage = "Семья не найдена! Создайте сначала семью!";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Disabled
    @Test
    void deleteAllChildrenOlderThen() {
        //TODO  -- пока не могу придумать, как это протестировать :(
    }

    @Test
    void should_ReturnNumberOfFamilies_when_FamiliesAreExists() {
        assertEquals(11, familyService.getNumberOfFamilies());
    }

    @Test
    void should_ReturnFamily_when_IndexIsExist() {
        assertNotNull(familyService.getFamilyById(0));
    }

    @Test
    void should_ThrowTheError_when_IndexIsNotExist() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            familyService.getFamilyById(15);
        });

        String expectedMessage = "Семья с таким индексом не найдена";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void should_getAFamilyPet_when_GetPetsReceiveValidIndex() {
        familyService.setPet(10, fish);
        assertTrue(familyService.getPets(10).contains(fish));
    }

    @Test
    void should_ThrowTheError_when_GetPetsReceiveInvalidIndex() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            familyService.getPets(15);
        });

        String expectedMessage = "Семья с таким индексом не найдена";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void should_IncreaseAFamilyPetSet_when_GetPetsReceiveValidIndex() {
        familyService.setPet(10, fish);
        assertEquals(1, familyService.getPets(10).size());
    }

    @Test
    void should_ThrowTheError_when_SetPetsReceiveInvalidIndex() {
        Pet pet = new Dog();
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            familyService.setPet(15, pet);
        });

        String expectedMessage = "Семья с таким индексом не найдена";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @AfterEach
    void PurgeFamilies() {
        familyService.deleteAllFamilies();
    }
}