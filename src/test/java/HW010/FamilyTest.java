package HW010;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.assertEquals;

class FamilyTest {
    Man testedFather = new Man("Mike", "Dow", "30/01/1955");
    Woman testedMother = new Woman("Joan", "Dow", "29/03/1960");
    Human child = new Human("John", "Dow", "01/10/1980");
    Family testedFamily = new Family(testedMother, testedFather);

    FamilyTest() throws ParseException {
    }

    @BeforeEach
    void init() throws ParseException {
        for (int i = 0; i < 4; i++) {
            testedFamily.addChild(new Human("Name" + i, "Surname" + i, "10" + i + "/" + "01" + i + "/" + "1980" + i));
        }
        testedFamily.addChild(child);
    }

    @Test
    void when_Added5Children_then_ChildrenArraySizeMust5() {
        testedFamily.addChild(child);
        assertEquals(6, testedFamily.getChildren().size());
    }

    @Test
    void when_removeChildByIndex_then_ChildrenArraySizeMustBeDecreased() {
        testedFamily.deleteChildByIndex(1);
        assertEquals(4, testedFamily.getChildren().size());
    }

    @Test
    void when_removeChildByWrongIndex_then_ChildrenArraySizeMustBeUnchanged() {
        testedFamily.deleteChildByIndex(10);
        assertEquals(5, testedFamily.getChildren().size());
    }

    @Test
    void when_removeChild_then_ChildrenArraySizeMustBeDecreased() {
        testedFamily.deleteChild(child);
        int index = -1;
        for (int i = 0; i < testedFamily.getChildren().size(); i++) {
            if (testedFamily.getChildren().contains(child)) {
                index = i;
                break;
            }
        }
        assertEquals(4, testedFamily.getChildren().size());
        assertEquals(-1, index);
    }

    @Test
    void when_removeChildThatNotExist_then_ChildrenArraySizeMustBeUnchanged() throws ParseException {
        Human testedChild = new Human("John", "Connor", "10/10/1985");
        testedFamily.deleteChild(testedChild);
        assertEquals(5, testedFamily.getChildren().size());
    }

    @Test
    void countFamily() {
        assertEquals(7, testedFamily.countFamily());
    }

    @Test
    void testToString() {
        testedFamily.setChildren(new ArrayList<>());
        testedFamily.addChild(child);
        String expectedString = "Family: Dow\n" +
                "Father: Man {name = Mike, surname = Dow, birthday = 30/01/1955, lifetime: Прожил 66 лет, 6 месяцев и 11 дней, iq = 0, schedule = {}}\n" +
                "Mother: Woman {name = Joan, surname = Dow, birthday = 29/03/1960, lifetime: Прожил 61 лет, 4 месяцев и 11 дней, iq = 0, schedule = {}}\n" +
                "Children: [Human {name = John, surname = Dow, birthday = 01/10/1980, lifetime: Прожил 40 лет, 10 месяцев и 5 дней, iq = 0, schedule = {}}]\n" +
                "Pet: The family has no pet\n";
        assertEquals(expectedString, testedFamily.toString());
    }

    @AfterEach
    void purgeTestedFamilyObject() {
        testedFamily.setChildren(new ArrayList<>());
    }
}
