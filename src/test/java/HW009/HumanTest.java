package HW009;

import org.junit.jupiter.api.Test;

import java.text.ParseException;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class HumanTest {

    Man testedHuman = new Man("John", "Dow", "07/12/1980");
    Pet testedPet = new RoboCat(Species.ROBOCAT, "Kitty", 5, 99, new HashSet<>());

    HumanTest() throws ParseException {
    }

    @Test
    void testToString() {
        String expectedString = "Man {name = John, surname = Dow, birthday = 07/12/1980, lifetime: Прожил 40 лет, 7 месяцев и 28 дней, iq = 0, schedule = {}}";
        assertEquals(expectedString, testedHuman.toString());
    }

    @Test
    void should_returnTrue_when_timeToFeedIsTrue() {
        testedHuman.addPet(testedPet);
        assertTrue(testedHuman.feedPet(true, testedPet));
    }
}