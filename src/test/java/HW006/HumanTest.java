package HW006;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class HumanTest {

    private static final Human testedHuman = new Man("John", "Dow", 1980);
    private static final Pet testedPet = new RoboCat(Species.ROBOCAT, "Kitty", 5, 99, new String[]{});

    @Test
    void testToString() {
        String expectedString = "Man {name = John, surname = Dow, year = 1980, iq = 0, schedule = null}";
        assertEquals(expectedString, testedHuman.toString());
    }

    @Test
    void should_returnTrue_when_timeToFeedIsTrue() {
        testedHuman.setPet(testedPet);
        assertTrue(testedHuman.feedPet(true));
    }
}