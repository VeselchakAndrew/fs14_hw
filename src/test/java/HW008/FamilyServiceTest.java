package HW008;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;


import static org.junit.jupiter.api.Assertions.*;

class FamilyServiceTest {

    FamilyService familyService = new FamilyService();
    Man man = new Man("John", "Dow", 1975);
    Woman woman = new Woman("Jane", "Dow", 1980);
    Human child = new Human("Miranda", "Dow", 1997);
    Family family = new Family(woman, man);

    Man manEmpty = new Man();
    Woman womanEmpty = new Woman();
    Family familyEmpty = new Family(womanEmpty, manEmpty);

    Pet fish = new Fish(Species.FISH, "Goldy");

    @BeforeEach
    void init() {
        for (int i = 0; i < 10; i++) {
            Man man = new Man("FatherName" + i, "Surname" + i, 1980 + i);
            Woman woman = new Woman("MotherName" + i, "Surname" + i, 1980 + i);
            Human child = new Human("ChildName" + i, "Surname" + i, 2000 + i);
            Family family = new Family(woman, man);
            family.addChild(child);
            familyService.saveFamily(family);
        }

        Human child1 = new Human("Bill", "Dow", 2001);
        Human child2 = new Human("Bob", "Dow", 2003);
        Human child3 = new Human("Jane", "Dow", 2005);
        Human child4 = new Human("Jil", "Dow", 2007);
        family.addChild(child1);
        family.addChild(child2);
        family.addChild(child3);
        family.addChild(child4);
        familyService.saveFamily(family);
    }

    @Test
    void should_returnAllFamilies_when_FamiliesNotEmpty() {
        assertEquals(11, familyService.getAllFamilies().size());
    }

    @Test
    void should_displayAllFamilies() {
        familyService.deleteAllFamilies();
        familyService.saveFamily(family);
        System.out.println(family);
        String expectedString = "Family: Dow\n" +
                "Father: Man {name = John, surname = Dow, year = 1975, iq = 0, schedule = {}}\n" +
                "Mother: Woman {name = Jane, surname = Dow, year = 1980, iq = 0, schedule = {}}\n" +
                "Children: " +
                "[Human {name = Bill, surname = Dow, year = 2001, iq = 0, schedule = {}}, " +
                "Human {name = Bob, surname = Dow, year = 2003, iq = 0, schedule = {}}, " +
                "Human {name = Jane, surname = Dow, year = 2005, iq = 0, schedule = {}}, " +
                "Human {name = Jil, surname = Dow, year = 2007, iq = 0, schedule = {}}]\n" +
                "Pet: The family has no pet\n";
        assertEquals(expectedString, familyService.displayAllFamilies());
    }

    @Test
    void should_returnFamiliesBiggerThanNumber() {
        int numberOfMemberOfFamily = 4;
        assertEquals(1, familyService.getFamiliesBiggerThan(numberOfMemberOfFamily).size());

    }

    @Test
    void should_ReturnFamiliesLessThanNumber() {
        int numberOfMemberOfFamily = 4;
        assertEquals(10, familyService.getFamiliesLessThan(numberOfMemberOfFamily).size());
    }

    @Test
    void countFamiliesWithMemberNumber() {
        family.addChild(child);
        familyService.saveFamily(family);
        int numberOfMemberOfFamily = 7;
        assertEquals(1, familyService.countFamiliesWithMemberNumber(numberOfMemberOfFamily).size());

    }

    @Test
    void should_IncreaseFamilySize_when_AddedNewFamily() {

        familyService.saveFamily(familyEmpty);
        assertEquals(12, familyService.getAllFamilies().size());
    }

    @Test
    void should_FamilySizeNotChanged_when_FamilyUpdated() {
        Human child = new Human("Bob", "Dow", 2010);
        family.addChild(child);
        familyService.saveFamily(family);
        assertEquals(11, familyService.getAllFamilies().size());
    }

    @Test
    void should_DecreaseFamiliesSize_when_deletedIndexIsExist() {
        familyService.deleteFamilyByIndex(0);
        assertEquals(10, familyService.getAllFamilies().size());
    }

    @Test
    void should_ReturnTrue_when_deletedIndexIsExist() {
        assertTrue(familyService.deleteFamilyByIndex(0));
    }

    @Test
    void should_FamiliesSizeNotChanged_when_deletedIndexIsNotExist() {
        familyService.deleteFamilyByIndex(15);
        assertEquals(11, familyService.getAllFamilies().size());
    }

    @Test
    void should_ReturnFalse_when_deletedIndexIsNotExist() {
        assertFalse(familyService.deleteFamilyByIndex(15));
    }

    @Test
    void should_DecreaseFamiliesSize_when_deletedFamilyIsExist() {
        familyService.deleteFamily(family);
        assertEquals(10, familyService.getAllFamilies().size());
    }

    @Test
    void should_ReturnTrue_when_deletedFamilyIsExist() {
        familyService.saveFamily(family);
        assertTrue(familyService.deleteFamily(family));
    }

    @Test
    void should_FamiliesSizeNotChanged_when_deletedFamilyIsExist() {
        familyService.deleteFamily(family);
        assertEquals(10, familyService.getAllFamilies().size());
    }

    @Test
    void should_ReturnTrue_when_deletedFamilyIsNotExist() {
        familyService.deleteAllFamilies();
        assertFalse(familyService.deleteFamily(family));
    }

    @Test
    void should_AddNewBornChild_when_FamilyIsExist() {
        familyService.bornChild(family);
        assertEquals(5, familyService.getFamilyById(10).getChildren().size());
    }

    @Test
    void should_ThrowTheError_when_FamilyForNewBornIsNotExist() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            familyService.bornChild(familyEmpty);
        });

        String expectedMessage = "Сначала надо создать семью!";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }


    @Test
    void should_AddAdoptChild_when_FamilyIsExist() {
        Human adoptedChild = new Human();
        familyService.adoptChild(family, adoptedChild);
        assertEquals(5, familyService.getFamilyById(10).getChildren().size());
    }

    @Test
    void should_ThrowTheError_when_FamilyForAdoptIsNotExist() {

        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            familyService.adoptChild(familyEmpty, child);
        });

        String expectedMessage = "Семья не найдена! Создайте сначала семью!";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Disabled
    @Test
    void deleteAllChildrenOlderThen() {
        //TODO  -- пока не могу придумать, как это протестировать :(
    }

    @Test
    void should_ReturnNumberOfFamilies_when_FamiliesAreExists() {
        assertEquals(11, familyService.getNumberOfFamilies());
    }

    @Test
    void should_ReturnFamily_when_IndexIsExist() {
        assertNotNull(familyService.getFamilyById(0));
    }

    @Test
    void should_ThrowTheError_when_IndexIsNotExist() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            familyService.getFamilyById(15);
        });

        String expectedMessage = "Семья с таким индексом не найдена";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void should_getAFamilyPet_when_GetPetsReceiveValidIndex() {
        familyService.setPet(10, fish);
        assertTrue(familyService.getPets(10).contains(fish));
    }

    @Test
    void should_ThrowTheError_when_GetPetsReceiveInvalidIndex() {
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            familyService.getPets(15);
        });

        String expectedMessage = "Семья с таким индексом не найдена";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @Test
    void should_IncreaseAFamilyPetSet_when_GetPetsReceiveValidIndex() {
        familyService.setPet(10, fish);
        assertEquals(1, familyService.getPets(10).size());
    }

    @Test
    void should_ThrowTheError_when_SetPetsReceiveInvalidIndex() {
        Pet pet = new Dog();
        Exception exception = assertThrows(IllegalArgumentException.class, () -> {
            familyService.setPet(15, pet);
        });

        String expectedMessage = "Семья с таким индексом не найдена";
        String actualMessage = exception.getMessage();
        assertTrue(actualMessage.contains(expectedMessage));
    }

    @AfterEach
    void PurgeFamilies() {
        familyService.deleteAllFamilies();
    }
}