package HW008;

import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

class CollectionFamilyDaoTest {

    private final List<Family> families = new ArrayList<>();
    CollectionFamilyDao familyDao = new CollectionFamilyDao();
    Man man = new Man("John", "Dow", 1975);
    Woman woman = new Woman("Jane", "Dow", 1980);
    Family family = new Family(woman, man);

    @BeforeEach
    void init() {
        for (int i = 0; i < 10; i++) {
            Man man = new Man("FatherName" + i, "Surname" + i, 1980 + i);
            Woman woman = new Woman("MotherName" + i, "Surname" + i, 1980 + i);
            Human child = new Human("ChildName" + i, "Surname" + i, 2000 + i);
            Family family = new Family(woman, man);
            family.addChild(child);
            familyDao.saveFamily(family);
        }
    }

    @Test
    void should_returnAllFamilies_when_FamiliesIsNotEmpty() {
        assertEquals(10, familyDao.getAllFamilies().size());
    }

    @Test
    void should_returnZero_when_FamiliesIsEmpty() {
        familyDao.deleteAllFamilies();
        assertEquals(0, familyDao.getAllFamilies().size());
    }

    @Test
    void should_IncreaseFamilySize_when_AddedNewFamily() {
        familyDao.saveFamily(family);
        assertEquals(11, familyDao.getAllFamilies().size());
    }

    @Test
    void should_FamilySizeNotChanged_when_FamilyUpdated() {
        familyDao.saveFamily(family);
        Human child = new Human("Bob", "Dow", 2010);
        family.addChild(child);
        familyDao.saveFamily(family);
        assertEquals(11, familyDao.getAllFamilies().size());
    }

    @Test
    void should_DecreaseFamiliesSize_when_deletedIndexIsExist() {
        familyDao.deleteFamily(0);
        assertEquals(9, familyDao.getAllFamilies().size());
    }

    @Test
    void should_ReturnTrue_when_deletedIndexIsExist() {
        assertTrue(familyDao.deleteFamily(0));
    }

    @Test
    void should_FamiliesSizeNotChanged_when_deletedIndexIsNotExist() {
        familyDao.deleteFamily(11);
        assertEquals(10, familyDao.getAllFamilies().size());
    }

    @Test
    void should_ReturnFalse_when_deletedIndexIsNotExist() {
        assertFalse(familyDao.deleteFamily(11));
    }

    @Test
    void should_DecreaseFamiliesSize_when_deletedFamilyIsExist() {
        familyDao.deleteFamily(family);
        assertEquals(10, familyDao.getAllFamilies().size());
    }

    @Test
    void should_ReturnTrue_when_deletedFamilyIsExist() {
        familyDao.saveFamily(family);
        assertTrue(familyDao.deleteFamily(family));
    }

    @Test
    void should_FamiliesSizeNotChanged_when_deletedFamilyIsExist() {
        familyDao.deleteFamily(family);
        assertEquals(10, familyDao.getAllFamilies().size());
    }

    @Test
    void should_ReturnTrue_when_deletedFamilyIsNotExist() {
        assertFalse(familyDao.deleteFamily(family));
    }


    @AfterEach
    void PurgeFamilies() {
        families.clear();
    }
}