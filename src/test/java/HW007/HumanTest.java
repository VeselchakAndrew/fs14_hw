package HW007;

import org.junit.jupiter.api.Test;

import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class HumanTest {

    private static final Human testedHuman = new Man("John", "Dow", 1980);
    private static final Pet testedPet = new RoboCat(Species.ROBOCAT, "Kitty", 5, 99, new HashSet<>());

    @Test
    void testToString() {
        String expectedString = "Man {name = John, surname = Dow, year = 1980, iq = 0, schedule = {}}";
        assertEquals(expectedString, testedHuman.toString());
    }

    @Test
    void should_returnTrue_when_timeToFeedIsTrue() {
        testedHuman.addPet(testedPet);
        assertTrue(testedHuman.feedPet(true, testedPet));
    }
}