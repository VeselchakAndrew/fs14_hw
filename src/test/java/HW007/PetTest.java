package HW007;

import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.HashSet;

import static org.junit.jupiter.api.Assertions.assertEquals;

class PetTest {

    @Test
    void should_returnCorrectString_when_objectCreatedWithAllFields() {
        Pet testedAnimal = new DomesticCat(Species.CAT, "Kitty", 2, 99, new HashSet<>(Arrays.asList("eat", "sleep", "foul")));
        String expectedString = "CAT {nickname = Kitty, age = 2, trickLevel = 99, habits = [sleep, foul, eat], Умеет летать: false, Количество ног: 4, Имеет мех: true}";
        assertEquals(expectedString, testedAnimal.toString());
    }

    @Test
    void should_returnCorrectString_when_objectCreatedWithSpeciesAndNicknameFields() {
        Pet testedAnimal = new Fish(Species.FISH, "Goldy");
        String expectedString = "FISH {nickname = Goldy, age = 0, trickLevel = 0, habits = null, Умеет летать: false, Количество ног: 0, Имеет мех: false}";
        assertEquals(expectedString, testedAnimal.toString());
    }
}