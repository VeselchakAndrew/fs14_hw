package HW005;

public enum Species {
    DOG(false, 4, true),
    CAT(false, 4, true),
    FISH(false, 0, false),
    BIRD(true, 2, false),
    SNAKE(false, 0, false);

    boolean canFly;
    int numberOfLegs;
    boolean hasFur;

    Species(Boolean canFly, int numberOfLegs, boolean hasFur) {
        this.canFly = canFly;
        this.numberOfLegs = numberOfLegs;
        this.hasFur = hasFur;
    }
}


