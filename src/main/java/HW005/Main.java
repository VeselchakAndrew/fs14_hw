package HW005;

public class Main {
    public static void main(String[] args) {
        Pet dog = new Pet(Species.DOG, "Doggy", 10, 51, new String[]{"eat", "drink", "sleep"});
        Human mother = new Human("Jane", "Corleone", 1970);
        Human father = new Human("Vito", "Corleone", 1965);
        Human child = new Human("Antonio", "Corleone", 1995);
        String[][] schedule = {{DayOfWeek.MONDAY.name(), "Got to work"}, {DayOfWeek.THURSDAY.name(), "Mee with John"}};

        Human child2 = new Human("Mike", "Corleone", 1999);
        Human child3 = new Human("Sarah", "Corleone", 1992);
        Human child4 = new Human("Ben", "Corleone", 1997);
        Human child5 = new Human("Luka", "Corleone", 1994);

        System.out.println("==========================");
        System.out.println(child);
        System.out.println("==========================");

        Family corleone = new Family(mother, father);

        corleone.setPet(dog);
        corleone.addChild(child);
        corleone.addChild(child2);
        corleone.addChild(child3);
        corleone.addChild(child4);
        corleone.addChild(child5);
        System.out.println(corleone);
        System.out.println("********************************************\n");
        System.out.println(corleone.countFamily());
        System.out.println(child.getFamily());
        corleone.deleteChildByIndex(3);
        corleone.deleteChild(child5);
        System.out.println(corleone);
        System.out.println(corleone.getChildrenInfo());
        System.out.println(corleone.describePet());
        System.out.println(child.getPet());
        System.out.println("Family after child deleting: \n" + corleone.getChildrenInfo());
        System.out.println(corleone.countFamily());

        boolean petIsFed = child.feedPet(false);
        System.out.println(petIsFed);
        System.out.println(dog);
        child.setSchedule(schedule);
        System.out.println(child);
        System.out.println("=============================");
        System.out.println("Corleone family");
        System.out.println(corleone);
        System.out.println("=============================");


//        for (int i = 0; i < 10_000_000; i++) {
//            Human human = new Human();
//        }

    }
}
