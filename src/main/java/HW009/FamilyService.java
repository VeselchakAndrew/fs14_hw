package HW009;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

public class FamilyService {
    private final CollectionFamilyDao familyDao = new CollectionFamilyDao();
    private final SimpleDateFormat sdf = new SimpleDateFormat("yyyy");

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    public String displayAllFamilies() {
        String allFamiliesInfo = "";
        List<Family> families = getAllFamilies();
        for (Family family : families) {
            allFamiliesInfo += family;
        }
        return allFamiliesInfo;
    }

    public void saveFamily(Family family) {
        familyDao.saveFamily(family);
    }

    public List<Family> getFamiliesBiggerThan(int numberOfFamilyMembers) {
        List<Family> familiesBiggerThan = new ArrayList<>();
        List<Family> families = getAllFamilies();
        for (Family family : families) {
            if (family.countFamily() > numberOfFamilyMembers) {
                familiesBiggerThan.add(family);
            }
        }
        return familiesBiggerThan;
    }

    public List<Family> getFamiliesLessThan(int numberOfFamilyMembers) {
        List<Family> familiesLessThan = new ArrayList<>();
        List<Family> families = getAllFamilies();
        for (Family family : families) {
            if (family.countFamily() < numberOfFamilyMembers) {
                familiesLessThan.add(family);
            }
        }
        return familiesLessThan;
    }

    public List<Family> countFamiliesWithMemberNumber(int numberOfFamilyMembers) {
        List<Family> familiesWithMemberNumber = new ArrayList<>();
        List<Family> families = getAllFamilies();
        for (Family family : families) {
            if (family.countFamily() == numberOfFamilyMembers) {
                familiesWithMemberNumber.add(family);
            }
        }
        return familiesWithMemberNumber;
    }

    public void createNewFamily(Man man, Woman woman) {
        Family family = new Family(woman, man);
        if (getAllFamilies().contains(family)) {
            System.out.println("Такая семья уже есть!");
        } else {
            familyDao.saveFamily(family);
        }
    }

    public Boolean deleteFamilyByIndex(int index) {
        return familyDao.deleteFamily(index);
    }

    public Boolean deleteFamily(Family family) {
        return familyDao.deleteFamily(family);
    }

    public void deleteAllFamilies() {
        familyDao.deleteAllFamilies();
    }

    public void bornChild(Family family) throws ParseException {
        if (!getAllFamilies().contains(family)) {
            throw new IllegalArgumentException("Сначала надо создать семью!");
        } else {
            try {
                family.bornChild();
                familyDao.saveFamily(family);
            } catch (ParseException e) {
                e.printStackTrace();
            }

        }
    }

    public void adoptChild(Family family, Human child) {
        if (getAllFamilies().contains(family)) {
            family.addChild(child);
        } else {
            throw new IllegalArgumentException("Семья не найдена! Создайте сначала семью!");
        }
    }

    public void deleteAllChildrenOlderThen(int age) throws ParseException {
        List<Family> families = getAllFamilies();
        for (Family family : families) {
            List<Human> children = new ArrayList<>(family.getChildren());
            for (Human child : children) {
                SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
                Date childYearOfBirth = sdf.parse(child.getBirthDate());
                sdf = new SimpleDateFormat("yyyy");
                if ((LocalDate.now().getYear() - Integer.parseInt(sdf.format(childYearOfBirth))) > age) {
                    family.deleteChild(child);
                    familyDao.saveFamily(family);
                }
            }
        }
    }

    public int getNumberOfFamilies() {
        return getAllFamilies().size();
    }

    public Family getFamilyById(int index) {
        if (familyDao.getFamilyByIndex(index) == null) {
            throw new IllegalArgumentException("Семья с таким индексом не найдена");
        } else {
            return getAllFamilies().get(index);
        }
    }

    public Set<Pet> getPets(int index) {
        if (familyDao.getFamilyByIndex(index) == null) {
            throw new IllegalArgumentException("Семья с таким индексом не найдена");
        } else {
            return familyDao.getFamilyByIndex(index).getPet();
        }
    }

    public void setPet(int index, Pet pet) {
        if (familyDao.getFamilyByIndex(index) == null) {
            throw new IllegalArgumentException("Семья с таким индексом не найдена");
        } else {
            familyDao.getFamilyByIndex(index).addPet(pet);
        }
    }
}
