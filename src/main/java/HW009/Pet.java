package HW009;

import java.util.Objects;
import java.util.Set;

public abstract class Pet {
    private Species species = Species.UNKNOWN;
    private String nickname;
    private int age;
    private int trickLevel;
    private Set<String> habits;

//    static {
//        System.out.println("Class Pet is loading...");
//    }
//
//    {
//        System.out.printf("Class %s successfully created\n", this.getClass().getSimpleName());
//    }


    public Pet(Species species, String nickname, int age, int trickLevel, Set<String> habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    public Pet(Species species, String nickname) {
        this.species = species;
        this.nickname = nickname;
    }

    public Pet() {

    }

    //Getters and setters
    public Species getSpecies() {
        return species;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public Set<String> getHabits() {
        return habits;
    }

    public void setHabits(Set<String> habits) {
        this.habits = habits;
    }

    //Methods
    public void eat() {
        System.out.println("Я кушаю");
    }

    public abstract void respond();

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return age == pet.age && trickLevel == pet.trickLevel && Objects.equals(species, pet.species) && Objects.equals(nickname, pet.nickname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(species, nickname, age, trickLevel);
    }

    @Override
    public String toString() {
        return species + " {nickname = " + nickname
                + ", age = " + age
                + ", trickLevel = " + trickLevel
                + ", habits = " + habits
                + ", Умеет летать: " + species.canFly
                + ", Количество ног: " + species.numberOfLegs
                + ", Имеет мех: " + species.hasFur + "}";
    }

    @Override
    protected void finalize() {
        System.out.println("Объект " + this.getClass().getSimpleName() + " удален");
    }
}

