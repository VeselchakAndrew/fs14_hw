package HW009;

import java.text.ParseException;

public interface HumanCreator {
    Human bornChild() throws ParseException;
}
