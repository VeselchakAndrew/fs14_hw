package HW009;

import java.text.ParseException;
import java.util.Arrays;
import java.util.HashSet;

public class Main {
    public static void main(String[] args) throws ParseException {
        FamilyController familyController = new FamilyController();

        Pet dog = new Dog(Species.DOG, "Doggy", 10, 51, new HashSet<>(Arrays.asList("eat", "drink", "sleep")));
        Pet fish = new Fish(Species.FISH, "Goldy", 1, 10, new HashSet<>(Arrays.asList("eat", "swim", "sleep")));
        Pet cat = new DomesticCat(Species.DOMESTIC_CAT, "Kitty", 3, 99, new HashSet<>(Arrays.asList("eat", "sleep", "foul")));

        Woman mother = new Woman("Jane", "Corleone", "10/12/1970");
        Man father = new Man("Vito", "Corleone", "03/08/1965");
        Family corleone = new Family(mother, father);
//
//
        Human child = new Man("Antonio", "Corleone", "28/12/1995");
        Human child2 = new Man("Mike", "Corleone", "01/02/1999");
        Human child3 = new Human("Sarah", "Corleone", "10/10/1992");
        Human child4 = new Man("Ben", "Corleone", "04/07/1997");
        Human child5 = new Man("Luka", "Corleone", "24/07/1994");

        corleone.addChild(child);
        corleone.addChild(child2);
        corleone.addChild(child3);
        corleone.addChild(child4);
        corleone.addChild(child5);

//        // Save family
        familyController.saveFamily(corleone);

//        //Adopt child
        Human adoptedChild = new Human("Jacky", "Moon", "10/12/2009", 130);
        familyController.adoptChild(familyController.getFamilyById(0), adoptedChild);

        System.out.println(familyController.getFamilyById(0).getChildren());
//
//
        System.out.println(child3.getBirthDate());
        System.out.println(child3.describeAge());
        System.out.println(child4.getBirthDate());
        System.out.println(child4.describeAge());



    }
}
