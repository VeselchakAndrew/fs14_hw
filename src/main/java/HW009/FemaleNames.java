package HW009;

public enum FemaleNames {
    MELANIE,
    FLORENCE,
    AGATHA,
    ZOE,
    REBECCA,
    RUTH,
    BARBARA,
    AMANDA,
    VICTORIA,
    IRENE,
    MIRANDA,
    BRIDGET;
}
