package HW009;

import java.util.Set;

public class Dog extends Pet implements PetFoul {

    private Species species = Species.DOG;

    public Dog(Species species, String nickname, int age, int trickLevel, Set<String> habits) {
        super(species, nickname, age, trickLevel, habits);
    }

    public Dog(Species species, String nickname) {
        super(species, nickname);
    }

    public Dog() {

    }

    @Override
    public void respond() {
        System.out.printf("Woof, хозяин. Я - %s. Я соскучился!\n", super.getNickname());
    }

    @Override
    public void makeFoul() {
        System.out.println("Woof woof! Нужно хорошо замести следы...");
    }

    @Override
    public Species getSpecies() {
        return this.species;
    }

    @Override
    public void setSpecies(Species species) {
        this.species = species;
    }
}
