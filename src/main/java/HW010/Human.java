package HW010;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class Human {
    private String name;
    private String surname;
    private long birthDateInMS;
    private int iqLevel;
    private Map<String, String> schedule = new HashMap<>();
    private Family family;
    private Set<Pet> pets = new HashSet<>();
    private Human father;
    private Human mother;

    private static final Random random = new Random();
    private static final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    private static final Date date = new Date();

//    static {
//        System.out.println("Class Human is loading...");
//    }
//
//    {
//        System.out.printf("Class %s successfully created\n", this.getClass().getSimpleName());
//    }

    public Human() {
    }

    public Human(String name, String surname, String birthDate) throws ParseException {
        this.name = name;
        this.surname = surname;
        this.birthDateInMS = convertStringToMS(birthDate);
    }

    public Human(String name, String surname, String birthDate, int iqLevel) throws ParseException {
        this.name = name;
        this.surname = surname;
        this.birthDateInMS = convertStringToMS(birthDate);
        this.iqLevel = iqLevel;
    }

    public Human(String name, String surname, String birthDate, int iqLevel, Pet pet, Human mother, Human father, HashMap<String, String> schedule) throws ParseException {
        this.name = name;
        this.surname = surname;
        this.birthDateInMS = convertStringToMS(birthDate);
        this.iqLevel = iqLevel;
        this.schedule = schedule;
    }

    //Getters and Setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public String getBirthDate() {
        return convertMStoString(birthDateInMS);
    }

    public void setBirthDate(String birthDate) throws ParseException {
        this.birthDateInMS = convertStringToMS(birthDate);
    }

    public int getIqLevel() {
        return iqLevel;
    }

    public void setIqLevel(int iqLevel) {
        this.iqLevel = iqLevel;
    }

    public Map<String, String> getSchedule() {
        return schedule;
    }

    public void setSchedule(Map<String, String> schedule) {
        this.schedule = schedule;
    }

    public String getFamily() {
        return family.parentsInfo();
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public Set<Pet> getPet() {
        return pets;
    }

    public void setPet(Set<Pet> pets) {
        this.pets = pets;
    }

    //Methods
    public boolean feedPet(boolean timeToFeed, Pet pet) {
        if (!pets.contains(pet)) {
            System.out.println("Не кормите чужих животных");
            return false;
        }
        boolean checkForTrick = false;
        if (!timeToFeed) {
            if (pet.getTrickLevel() > random.nextInt(100)) {
                checkForTrick = true;
            }

            if (checkForTrick | timeToFeed) {
                System.out.printf("Хм... покормлю ка я %s\n", pet.getNickname());
                return true;
            } else {
                System.out.printf("Думаю, %s не голоден.\n", pet.getNickname());
                return false;
            }
        }
        System.out.printf("Хм... покормлю ка я %s\n", pet.getNickname());
        return true;
    }

    public String describeAge() {
        long difference = date.getTime() - birthDateInMS;
        int day = 1000 * 60 * 60 * 24;
        int daysInDiff = (int) (difference / day);
        int years = daysInDiff / 365;
        int months = (daysInDiff - (years * 365)) / 30;
        int days = daysInDiff - (years * 365) - (months * 30);
        return "Прожил " + years + " лет, " + months + " месяцев и " + days + " дней";
    }


    public void greetPet() {
        if (pets.isEmpty()) {
            System.out.println("У меня нет домашнего питомца :(");
        } else {
            for (Pet pet : pets) {
                System.out.printf("Привет, %s\n", pet.getNickname());
            }
        }
    }

    public int getHumanAge() {
        long difference = date.getTime() - birthDateInMS;
        int day = 1000 * 60 * 60 * 24;
        int daysInDiff = (int) (difference / day);
        return daysInDiff / 365;
    }

    private String convertMStoString(long dateInMilliseconds) {
        Date date = new Date(dateInMilliseconds);
        return sdf.format(date);
    }

    private long convertStringToMS(String date) throws ParseException {
        if (date.equals("") || date == null) {
            throw new IllegalArgumentException("Некорректная дата! Дата должна быть в формате ДД/ММ/ГГГГ");
        }
        Date dateInMS = sdf.parse(date);
        return dateInMS.getTime();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return birthDateInMS == human.birthDateInMS && iqLevel == human.iqLevel && Objects.equals(name, human.name) && Objects.equals(surname, human.surname) && Objects.equals(family, human.family);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, birthDateInMS, iqLevel, family);
    }

    @Override
    public String toString() {
        String className = this.getClass().getSimpleName();
        return className + " {name = " + name + ", surname = " + surname +
                ", birthday = " + getBirthDate() + ", lifetime: " + describeAge() +
                ", iq = " + iqLevel + ", schedule = " + getSchedule() + "}";
    }

    @Override
    protected void finalize() {
        System.out.println("Объект " + this.getClass().getSimpleName() + " удален");
    }

    public void addPet(Pet pet) {
        pets.add(pet);
    }
}