package HW002;

import java.util.Random;
import java.util.Scanner;

public class HW002 {
    private static final Random random = new Random();
    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int horizontalDimension = 6;
        int verticalDimension = 6;
        int targetY = random.nextInt(horizontalDimension) - 1;
        int targetX = random.nextInt(verticalDimension) - 1;
        System.out.println(targetY + " " + targetX);
        System.out.println("All set. Get ready to rumble!");
        String[][] gameField = fillArray(horizontalDimension, verticalDimension);
        boolean isRunning = true;
        int shootY;
        int shootX;
        int count = 0;

        while (isRunning) {
            shootY = inputData(1, horizontalDimension - 1, "horizontal");
            shootX = inputData(1, verticalDimension - 1, "vertical");

            if (targetX == shootX && targetY == shootY) {
                gameField[shootX][shootY] = "X";
                count++;
                System.out.println("You have won!");
                System.out.printf("You make a %d attempts\n", count);
                printResult(gameField);
                isRunning = false;

            } else {
                gameField[shootX][shootY] = "*";
                count++;
                System.out.println("********************");
                System.out.println("You miss! Try again!");
                System.out.println("********************");
                printResult(gameField);
            }
        }
    }

    private static String[][] fillArray(int x, int y) {
        String[][] gameField = new String[x][y];
        for (int i = 0; i < gameField.length; i++) {
            for (int j = 0; j < gameField[i].length; j++) {
                if (i == 0) {
                    gameField[i][j] = Integer.toString(j);
                } else if (j == 0) {
                    gameField[i][j] = Integer.toString(i);
                } else {
                    gameField[i][j] = "-";
                }
            }
        }
        return gameField;
    }

    private static void printResult(String[][] arr) {
        for (String[] strings : arr) {
            for (String string : strings) {
                System.out.print(string + "\t");
            }
            System.out.println();
        }
    }

    public static int inputData(int min, int max, String direction) {
        int shoot;
        do {
            System.out.printf("Enter %s coord, (1 - 5): ", direction);
            shoot = scanner.nextInt();
            if (shoot < 1 || shoot > 5) {
                System.out.println("Must be in 1 to 5 range");
            }
        } while (shoot < min || shoot > max);
        return shoot;
    }
}