package HW003;

import java.util.Scanner;

public class HW003 {

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        String[][] schedule = new String[7][2];
        schedule[0][0] = "Sunday";
        schedule[0][1] = "do home work";
        schedule[1][0] = "Monday";
        schedule[1][1] = "go to courses; watch a film";
        schedule[2][0] = "Tuesday";
        schedule[2][1] = "go to walk";
        schedule[3][0] = "Wednesday";
        schedule[3][1] = "read the book";
        schedule[4][0] = "Thursday";
        schedule[4][1] = "meet with friends";
        schedule[5][0] = "Friday";
        schedule[5][1] = "drink beer with friends";
        schedule[6][0] = "Saturday";
        schedule[0][1] = "watch a study video; do home work";

        boolean isWorking = true;

        while (isWorking) {
            System.out.println("Enter day of week to view your schedule or\ntype \"change\" and day of week for change schedule:");
            String userInput = scanner.nextLine().toLowerCase().trim();
            String[] userCommands = stringHandler(userInput);

            if (userCommands[0].equals("exit")) {
                isWorking = false;
            } else if (userCommands[0].equals("change")) {
                System.out.printf("Enter new schedule for %s:\n", firstUpperCase(userCommands[1]));
                String newUserData = scanner.nextLine().toLowerCase().trim();
                changeUserSchedule(userCommands[1], schedule, newUserData);
            } else {
                showUserSchedule(userCommands[0], schedule);

            }
        }
    }

    private static String firstUpperCase(String word) {
        if (word == null || word.isEmpty()) return "";
        return word.substring(0, 1).toUpperCase() + word.substring(1);
    }

    private static String[] stringHandler(String userInput) {
        while (userInput.contains("  ")) {
            userInput = userInput.replace("  ", " ");
        }
        return userInput.split(" ");
    }

    private static void showUserSchedule(String userInput, String[][] schedule) {
        boolean dayOfWeekIsNotFind = true;
        for (int i = 0; i < schedule.length; i++) {
            if ((schedule[i][0].toLowerCase()).equals(userInput)) {
                System.out.printf("Your tasks for %s: %s\n", firstUpperCase(userInput), schedule[i][1]);
                dayOfWeekIsNotFind = false;
                break;
            }
        }
        if (dayOfWeekIsNotFind) {
            System.out.println("Sorry, I don't understand you, please try again.\nIf you want exit - type \"exit\"");
        }
    }

    private static void changeUserSchedule(String userInput, String[][] schedule, String newUserData) {
        boolean dayOfWeekIsNotFind = true;
        for (int i = 0; i < schedule.length; i++) {
            if ((schedule[i][0].toLowerCase()).equals(userInput)) {
                schedule[i][1] = newUserData;
                System.out.printf("New schedule for %s successfully saved", schedule[i][0]);
                dayOfWeekIsNotFind = false;
                break;
            }
        }
        if (dayOfWeekIsNotFind) {
            System.out.println("Sorry, I don't understand you, please try again.\nIf you want exit - type \"exit\"");
        }
    }
}


