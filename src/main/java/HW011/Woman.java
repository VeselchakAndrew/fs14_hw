package HW011;

import java.text.ParseException;
import java.util.HashMap;

public final class Woman extends Human {

    public Woman() {
    }

    public Woman(String name, String surname, String birthDate) throws ParseException {
        super(name, surname, birthDate);
    }

    public Woman(String name, String surname, String birthDate, int iqLevel) throws ParseException {
        super(name, surname, birthDate, iqLevel);
    }

    public Woman(String name, String surname, String birthDate, int iqLevel, Pet pet, Human mother, Human father, HashMap<String, String> schedule) throws ParseException {
        super(name, surname, birthDate, iqLevel, pet, mother, father, schedule);
    }

    public void makeUp() {
        System.out.println("Я умею делать мейк-ап");
    }

    @Override
    public void greetPet() {
        if (super.getPet().isEmpty()) {
            System.out.println("У меня нет домашнего питомца :(");
        } else {
            for (Pet pet : super.getPet()) {
                System.out.printf("Привет, %s, рада тебя видеть", pet.getNickname());
            }
        }
    }
}
