package HW011;

import java.text.ParseException;

public interface HumanCreator {
    Human bornChild() throws ParseException;
}
