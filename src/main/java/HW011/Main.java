package HW011;

import java.text.ParseException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class Main {
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws ParseException {

        FamilyController familyController = new FamilyController();

        boolean isRunning = true;

        while (isRunning) {
            System.out.println("============== Main menu ==============");
            System.out.println("1. Заполнить тестовыми данными");
            System.out.println("2. Отобразить весь список семей");
            System.out.println("3. Отобразить список семей, где количество людей больше заданного");
            System.out.println("4. Отобразить список семей, где количество людей меньше заданного");
            System.out.println("5. Подсчитать количество семей, где количество членов равно");
            System.out.println("5. Подсчитать количество семей, где количество членов равно");
            System.out.println("7. Удалить семью по индексу семьи в общем списке");
            System.out.println("8. Редактировать семью по индексу семьи в общем списке");
            System.out.println("9. Удалить всех детей старше возраста");
            System.out.println("0. Закончить работу");
            System.out.println("=======================================");
            System.out.println();
            int userInput = inputData();

            switch (userInput) {
                case 1: {
                    System.out.println("Введите количество тестовых записей: ");
                    int numberOfFamilies = inputData();
                    if (numberOfFamilies >= 0) {
                        familyController.makeTestFamilies(numberOfFamilies);
                        System.out.println("База заполнена тестовыми данными \n");
                    } else {
                        System.out.println("База не заполнена. Количество тестовых записей должно быть больше 0!");
                    }
                    continueWork();
                    break;
                }
                case 2: {
                    System.out.println("Количество семей - " + familyController.getAllFamilies().size() + "\n");
                    familyController.getAllFamilies().forEach(family -> {
                        System.out.println(family.prettyFormat());
                    });
                    continueWork();
                    break;
                }
                case 3: {
                    System.out.println("Введите количество членов семьи");
                    System.out.println(familyController.getFamiliesBiggerThan(inputData()));
                    continueWork();
                    break;
                }
                case 4: {
                    System.out.println("Введите количество членов семьи");
                    System.out.println(familyController.getFamiliesLessThan(inputData()));
                    continueWork();
                    break;
                }
                case 5: {
                    System.out.println("Введите количество членов семьи");
                    int numberOfCountOfFamily = inputData();
                    System.out.println("Количество семей, где количество членов, равно " + numberOfCountOfFamily);
                    System.out.println(familyController.countFamiliesWithMemberNumber(numberOfCountOfFamily).size());
                    continueWork();
                    break;
                }
                case 6: {
                    System.out.println("Введите имя мужчины");
                    String name = scanner.nextLine();
                    System.out.println("Введите фамилию мужчины");
                    String surname = scanner.nextLine();
                    System.out.println("Введите дату рождения мужчины в формате ДД/ММ/ГГГГ");
                    String dateOfBirth = scanner.nextLine();
                    System.out.println("Введите уровень IQ");
                    int IQ = scanner.nextInt();
                    Man man = new Man(name, surname, dateOfBirth, IQ);
                    System.out.println("Введите имя женщины");
                    name = scanner.nextLine();
                    System.out.println("Введите фамилию женщины");
                    surname = scanner.nextLine();
                    System.out.println("Введите дату рождения женщины в формате ДД/ММ/ГГГГ");
                    dateOfBirth = scanner.nextLine();
                    System.out.println("Введите уровень IQ");
                    IQ = scanner.nextInt();
                    Woman woman = new Woman(name, surname, dateOfBirth, IQ);
                    Family family = new Family(woman, man);
                    familyController.saveFamily(family);
                    break;
                }
                case 7: {
                    System.out.println("Введите индекс удаляемой семьи, от 0 до " + (familyController.getAllFamilies().size() - 1));
                    int index = inputData();
                    boolean isDone = familyController.deleteFamilyByIndex(index);
                    if (isDone) {
                        System.out.println("Семья под индексом " + index + " успешно удалена\n");
                    } else {
                        System.out.println("Семья под индексом " + index + " не найдена\n");
                    }
                    continueWork();
                    break;
                }
                case 8: {
                    System.out.println("1. Родить ребенка");
                    System.out.println("2. Усыновить ребенка");
                    System.out.println("3. Вернуться в главное меню");
                    int subMenuUserInput = inputData();
                    switch (subMenuUserInput) {
                        case 1: {
                            System.out.println("Введите индекс семьи, от 0 до " + (familyController.getAllFamilies().size() - 1));
                            int index = inputData();
                            Family family = familyController.getFamilyById(index);
                            familyController.bornChild(family);
                            System.out.println("Ребенок успешно родился :)");
                            System.out.println(familyController.getFamilyById(index));
                            continueWork();
                            break;
                        }
                        case 2: {
                            System.out.println("Введите индекс семьи, от 0 до " + (familyController.getAllFamilies().size() - 1));
                            int index = inputData();
                            Family family = familyController.getFamilyById(index);
                            System.out.println("Введите имя усыновляемого ребенка");
                            String name = scanner.nextLine();
                            System.out.println("Введите фамилию усыновляемого ребенка");
                            String surname = scanner.nextLine();
                            System.out.println("Введите дату рождения ребенка в формате ДД/ММ/ГГГГ");
                            String dateOfBirth = scanner.nextLine();
                            System.out.println("Введите желаемый уровень IQ");
                            int IQ = scanner.nextInt();
                            Human child = new Human(name, surname, dateOfBirth, IQ);
                            familyController.adoptChild(family, child);
                            System.out.println("Ребенок успешно усыновлен\n");
                            System.out.println(familyController.getFamilyById(index));
                            continueWork();
                            break;
                        }
                        case 3: {
                            System.out.println("Выходим в главное меню\n");
                            break;
                        }
                    }
                    break;
                }
                case 9: {
                    System.out.println("Введите желаемый возраст");
                    int ageOfDeletedChildren = inputData();
                    if (ageOfDeletedChildren >= 0) {
                        familyController.deleteAllChildrenOlderThen(ageOfDeletedChildren);
                        System.out.println("Все дети старше " + ageOfDeletedChildren + " успешно удалены из базы\n");
                    } else {
                        System.out.println("Возраст должен быть больше или равен 0!");
                    }
                    continueWork();
                    break;
                }
                case 0: {
                    System.out.println("Работа завершена");
                    isRunning = false;
                    break;
                }
                default: {
                    System.out.println("Команда не распознана\n");
                }
            }
        }
        scanner.close();


    }

    private static int inputData() {
        int userInput = -1;
        try {
            userInput = scanner.nextInt();

        } catch (InputMismatchException e) {
            scanner.nextLine();
            System.out.println("Введите число!");
        }
        return userInput;
    }

    private static void continueWork() {
        System.out.println("Для продолжения нажмите Enter");
        try {
            System.in.read();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}