package HW011;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.stream.Collectors;

public class FamilyService {
    private final CollectionFamilyDao familyDao = new CollectionFamilyDao();
    private final SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
    private final SimpleDateFormat sdfYear = new SimpleDateFormat("yyyy");
    private final Random random = new Random();

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    public void displayAllFamilies() {
        getAllFamilies().forEach(System.out::println);
    }

    public void saveFamily(Family family) {
        familyDao.saveFamily(family);
    }

    public List<Family> getFamiliesBiggerThan(int numberOfFamilyMembers) {
        return getAllFamilies()
                .stream()
                .filter(family -> family.countFamily() > numberOfFamilyMembers)
                .collect(Collectors.toList());
    }

    public List<Family> getFamiliesLessThan(int numberOfFamilyMembers) {
        return getAllFamilies()
                .stream()
                .filter(family -> family.countFamily() < numberOfFamilyMembers)
                .collect(Collectors.toList());

    }

    public List<Family> countFamiliesWithMemberNumber(int numberOfFamilyMembers) {
        return getAllFamilies()
                .stream()
                .filter(family -> family.countFamily() == numberOfFamilyMembers)
                .collect(Collectors.toList());
    }

    public void createNewFamily(Man man, Woman woman) {
        Family family = new Family(woman, man);
        if (getAllFamilies().contains(family)) {
            System.out.println("Такая семья уже есть!");
        } else {
            familyDao.saveFamily(family);
        }
    }

    public Boolean deleteFamilyByIndex(int index) {
        return familyDao.deleteFamily(index);
    }

    public Boolean deleteFamily(Family family) {
        return familyDao.deleteFamily(family);
    }

    public void deleteAllFamilies() {
        familyDao.deleteAllFamilies();
    }

    public void bornChild(Family family) throws ParseException {
        if (!getAllFamilies().contains(family)) {
            throw new IllegalArgumentException("Сначала надо создать семью!");
        } else {
            try {
                family.bornChild();
                familyDao.saveFamily(family);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        }
    }

    public void adoptChild(Family family, Human child) {
        if (family.getChildren().size() > 10) {
            throw new FamilyOverflowException("Достигнут предельный размер семьи");
        }
        if (getAllFamilies().contains(family)) {
            family.addChild(child);
        } else {
            throw new IllegalArgumentException("Семья не найдена! Создайте сначала семью!");
        }
    }

    public void deleteAllChildrenOlderThen(int age) throws ParseException {
        List<Family> families = getAllFamilies();
        for (Family family : families) {
            List<Human> result = family.getChildren().stream().filter(child -> child.getHumanAge() < age).collect(Collectors.toList());
            family.setChildren(result);
        }
    }

    public int getNumberOfFamilies() {
        return getAllFamilies().size();
    }

    public Family getFamilyById(int index) {
        if (familyDao.getFamilyByIndex(index) == null) {
            throw new IllegalArgumentException("Семья с таким индексом не найдена");
        } else {
            return getAllFamilies().get(index);
        }
    }

    public Set<Pet> getPets(int index) {
        if (familyDao.getFamilyByIndex(index) == null) {
            throw new IllegalArgumentException("Семья с таким индексом не найдена");
        } else {
            return familyDao.getFamilyByIndex(index).getPet();
        }
    }

    public void setPet(int index, Pet pet) {
        if (familyDao.getFamilyByIndex(index) == null) {
            throw new IllegalArgumentException("Семья с таким индексом не найдена");
        } else {
            familyDao.getFamilyByIndex(index).addPet(pet);
        }
    }

    private int getChildYearOfBirth(Human child) {
        int childYearOfBirth = 0;
        try {
            Date childDateOfBirth = sdf.parse(child.getBirthDate());
            childYearOfBirth = Integer.parseInt(sdfYear.format(childDateOfBirth));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return childYearOfBirth;
    }

    public void makeTestFamilies(int numberOfFamilies) throws ParseException {
        for (int i = 0; i < numberOfFamilies; i++) {
            int numberOfChild = random.nextInt(6);
            Man man = new Man("FatherName" + i, "Surname" + i, "10" + i + "/" + "01" + i + "/" + (1980 + i));
            Woman woman = new Woman("MotherName" + i, "Surname" + i, "10" + i + "/" + "01" + i + "/" + (1980 + i));
            Family family = new Family(woman, man);
            for (int j = 0; j < numberOfChild; j++) {
                Human child = new Human("ChildName" + i + "_" + j, "Surname" + i, "01" + j + "/" + "01" + j + "/" + (2000 + j));
                family.addChild(child);
            }
            saveFamily(family);
        }
    }
}
