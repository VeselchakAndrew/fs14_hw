package HW011;

public enum FemaleNames {
    MELANIE,
    FLORENCE,
    AGATHA,
    ZOE,
    REBECCA,
    RUTH,
    BARBARA,
    AMANDA,
    VICTORIA,
    IRENE,
    MIRANDA,
    BRIDGET;
}
