package HW008;

import java.util.*;

public class Family implements HumanCreator {
    private Woman mother;
    private Man father;
    private List<Human> children = new ArrayList<>();
    private Set<Pet> pets = new HashSet<>();

    private static final Random random = new Random();

//    static {
//        System.out.println("Class Family is loading...");
//    }
//
//    {
//        System.out.printf("Class %s successfully created\n", this.getClass().getSimpleName());
//    }

    public Family(Woman mother, Man father) {
        this.mother = mother;
        this.father = father;
    }

    //Getters and Setters
    public Human getMother() {
        return mother;
    }

    public void setMother(Woman mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Man father) {
        this.father = father;
    }

    public List<Human> getChildren() {
        return children;
    }

    public void setChildren(List<Human> children) {
        this.children = children;
    }

    public Set<Pet> getPet() {
        return pets;
    }

    public void setPet(Set<Pet> pets) {
        setPetToChildren(pets);
    }

    //Methods
    public void addChild(Human child) {
        children.add(child);
        child.setFamily(this);
        setPetToChildren(pets);
    }

    public void deleteChildByIndex(int index) {
        if (index > children.size()) {
            return;
        }
        children.remove(index);

    }

    public void deleteChild(Human childForDelete) {
        children.remove(childForDelete);
    }

    public int countFamily() {
        return children.size() + 2;
    }

    public void addPet(Pet pet) {
        this.pets.add(pet);
        setPetToChildren(pets);

    }

    public String parentsInfo() {
        return "Father: " + father +
                "\nMother: " + mother;
    }


    public String describePet() {
        String petTrick;
        if (pets.isEmpty()) {
            return "The family has no pet";
        }

        String petInfo = "";
        for (Pet pet : pets) {

            if (pet.getTrickLevel() > 50) {
                petTrick = "очень хитрый";
            } else {
                petTrick = "почти не хитрый";
            }

            petInfo += "У нас есть " + pet.getSpecies() + ", ему " + pet.getAge() + " лет, он " + petTrick;
        }
        return petInfo;
    }


    private void setPetToChildren(Set<Pet> pets) {

        if (children.size() > 0) {
            for (Human child : children) {
                child.setPet(pets);
            }
        }
    }

    public String getChildrenInfo() {
        StringBuilder childrenInfo = new StringBuilder("Children:\n");
        for (Human child : children) {
            childrenInfo.append(child.getName()).append(" ").append(child.getSurname()).append(" ").append(child.getYearOfBirth()).append("\n");
        }
        return childrenInfo.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return mother.equals(family.mother) && father.equals(family.father) && children.equals(family.children);
    }

    @Override
    public int hashCode() {
        return Objects.hash(mother, father, children);
    }

    @Override
    public String toString() {

        return "Family: " + father.getSurname() + "\nFather: " + father +
                "\nMother: " + mother +
                "\nChildren: " + children +
                "\nPet: " + describePet() + "\n";
    }

    @Override
    protected void finalize() {
        System.out.println("Объект " + this.getClass().getSimpleName() + " удален");
    }

    public Human bornChild() {
        int chance = random.nextInt(2);
        int iqLevel = (mother.getIqLevel() + father.getIqLevel()) / 2;
        int year = Calendar.getInstance().get(Calendar.YEAR);
        if (chance == 0) {
            int nameIndex = random.nextInt(MaleNames.values().length);
            Man newBornMan = new Man(MaleNames.values()[nameIndex].name(), father.getSurname(), year);
            newBornMan.setIqLevel(iqLevel);
            newBornMan.setFamily(this);
            newBornMan.setPet(pets);
            addChild(newBornMan);
            return newBornMan;

        } else {
            int nameIndex = random.nextInt(FemaleNames.values().length);
            Woman newBornWoman = new Woman(FemaleNames.values()[nameIndex].name(), father.getSurname(), year);
            newBornWoman.setIqLevel(iqLevel);
            newBornWoman.setFamily(this);
            newBornWoman.setPet(pets);
            addChild(newBornWoman);
            return newBornWoman;
        }
    }
}


