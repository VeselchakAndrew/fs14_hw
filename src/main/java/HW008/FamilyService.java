package HW008;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class FamilyService {
    CollectionFamilyDao familyDao = new CollectionFamilyDao();

    public List<Family> getAllFamilies() {
        return familyDao.getAllFamilies();
    }

    public String displayAllFamilies() {
        String allFamiliesInfo = "";
        List<Family> families = getAllFamilies();
        for (Family family : families) {
            allFamiliesInfo += family;
        }
        return allFamiliesInfo;
    }

    public void saveFamily(Family family) {
        familyDao.saveFamily(family);
    }

    public List<Family> getFamiliesBiggerThan(int numberOfFamilyMembers) {
        List<Family> familiesBiggerThan = new ArrayList<>();
        List<Family> families = getAllFamilies();
        for (Family family : families) {
            if (family.countFamily() > numberOfFamilyMembers) {
                familiesBiggerThan.add(family);
            }
        }
        return familiesBiggerThan;
    }

    public List<Family> getFamiliesLessThan(int numberOfFamilyMembers) {
        List<Family> familiesLessThan = new ArrayList<>();
        List<Family> families = getAllFamilies();
        for (Family family : families) {
            if (family.countFamily() < numberOfFamilyMembers) {
                familiesLessThan.add(family);
            }
        }
        return familiesLessThan;
    }

    public List<Family> countFamiliesWithMemberNumber(int numberOfFamilyMembers) {
        List<Family> familiesWithMemberNumber = new ArrayList<>();
        List<Family> families = getAllFamilies();
        for (Family family : families) {
            if (family.countFamily() == numberOfFamilyMembers) {
                familiesWithMemberNumber.add(family);
            }
        }
        return familiesWithMemberNumber;
    }

    public void createNewFamily(Man man, Woman woman) {
        Family family = new Family(woman, man);
        if (getAllFamilies().contains(family)) {
            System.out.println("Такая семья уже есть!");
        } else {
            familyDao.saveFamily(family);
        }
    }

    public Boolean deleteFamilyByIndex(int index) {
        return familyDao.deleteFamily(index);
    }

    public Boolean deleteFamily(Family family) {
        return familyDao.deleteFamily(family);
    }

    public void deleteAllFamilies() {
        familyDao.deleteAllFamilies();
    }

    public void bornChild(Family family) {
        if (!getAllFamilies().contains(family)) {
            throw new IllegalArgumentException("Сначала надо создать семью!");
        } else {
            family.bornChild();
            familyDao.saveFamily(family);
        }
    }

    public Family adoptChild(Family family, Human child) {
        if (getAllFamilies().contains(family)) {
            family.addChild(child);
            return family;
        } else {
            throw new IllegalArgumentException("Семья не найдена! Создайте сначала семью!");
        }
    }

    public void deleteAllChildrenOlderThen(int age) {
        List<Family> families = getAllFamilies();
        for (Family family : families) {
            List<Human> children = new ArrayList<>(family.getChildren());
            for (Human child : children) {
                if ((LocalDate.now().getYear() - child.getYearOfBirth()) > age) {
                    family.deleteChild(child);
                    familyDao.saveFamily(family);
                }
            }
        }
    }

    public int getNumberOfFamilies() {
        return getAllFamilies().size();
    }

    public Family getFamilyById(int index) {
        if (familyDao.getFamilyByIndex(index) == null) {
            throw new IllegalArgumentException("Семья с таким индексом не найдена");
        } else {
            return getAllFamilies().get(index);
        }
    }

    public Set<Pet> getPets(int index) {
        if (familyDao.getFamilyByIndex(index) == null) {
            throw new IllegalArgumentException("Семья с таким индексом не найдена");
        } else {
            return familyDao.getFamilyByIndex(index).getPet();
        }
    }

    public void setPet(int index, Pet pet) {
        if (familyDao.getFamilyByIndex(index) == null) {
            throw new IllegalArgumentException("Семья с таким индексом не найдена");
        } else {
            familyDao.getFamilyByIndex(index).addPet(pet);
        }
    }
}
