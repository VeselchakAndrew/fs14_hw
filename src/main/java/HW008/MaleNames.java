package HW008;

public enum MaleNames {
    JOHN,
    BOB,
    OLIVER,
    JACK,
    HARRY,
    JACOB,
    CHARLEY,
    THOMAS,
    GEORGE,
    OSCAR;
}
