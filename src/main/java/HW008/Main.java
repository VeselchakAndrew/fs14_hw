package HW008;

import java.util.Arrays;
import java.util.HashSet;

public class Main {
    public static void main(String[] args) {
        FamilyController familyController = new FamilyController();

        Pet dog = new Dog(Species.DOG, "Doggy", 10, 51, new HashSet<>(Arrays.asList("eat", "drink", "sleep")));
        Pet fish = new Fish(Species.FISH, "Goldy", 1, 10, new HashSet<>(Arrays.asList("eat", "swim", "sleep")));
        Pet cat = new DomesticCat(Species.DOMESTIC_CAT, "Kitty", 3, 99, new HashSet<>(Arrays.asList("eat", "sleep", "foul")));

        Woman mother = new Woman("Jane", "Corleone", 1970);
        Man father = new Man("Vito", "Corleone", 1965);
        Family corleone = new Family(mother, father);

        Woman mother2 = new Woman("Jane2", "Corleone2", 1970);
        Man father2 = new Man("Vito2", "Corleone2", 1965);
        Family corleone2 = new Family(mother2, father2);

        Woman mother3 = new Woman("Jane3", "Corleone3", 1970);
        Man father3 = new Man("Vito3", "Corleone3", 1965);
        Family corleone3 = new Family(mother3, father3);

        Woman mother4 = new Woman("Jane4", "Corleone4", 1970);
        Man father4 = new Man("Vito4", "Corleone4", 1965);


        Human child = new Man("Antonio", "Corleone", 1995);
        Human child2 = new Man("Mike", "Corleone", 1999);
        Human child3 = new Human("Sarah", "Corleone", 1992);
        Human child4 = new Man("Ben", "Corleone", 1997);
        Human child5 = new Man("Luka", "Corleone", 1994);

        corleone.addChild(child);
        corleone.addChild(child2);
        corleone.addChild(child3);
        corleone.addChild(child4);
        corleone.addChild(child5);

        corleone2.addChild(child);
        corleone2.addChild(child2);

        corleone3.addChild(child3);
        corleone3.addChild(child4);
        corleone3.addChild(child5);

        // Save family
        familyController.saveFamily(corleone);
        familyController.saveFamily(corleone2);
        familyController.saveFamily(corleone3);

        //Create new family
        familyController.createNewFamily(father4, mother4);

        //Update family
        corleone3.addChild(child);
        familyController.saveFamily(corleone3);


        //Display all families
        System.out.println("*******************************************************");
        System.out.println("Display all families");
        System.out.println(familyController.displayAllFamilies());
        System.out.println("*******************************************************");

        //Delete family by index
        familyController.deleteFamilyByIndex(0);

        //Delete family
        familyController.deleteFamily(corleone3);

        //Display all families
        System.out.println("*******************************************************");
        System.out.println("Display all families after deleting 2 items");
        System.out.println(familyController.displayAllFamilies());
        System.out.println("*******************************************************");

        //Born child
        familyController.bornChild(corleone2);

        //Adopt child
        familyController.adoptChild(familyController.getFamilyById(1), child);

        //Display all families
        System.out.println("*******************************************************");
        System.out.println("Display all families after bornChild() and adoptChild()");
        System.out.println(familyController.displayAllFamilies());
        System.out.println("*******************************************************");

        //Delete all children older then 10 years
        familyController.deleteAllChildrenOlderThen(10);

        //Display all families
        System.out.println("*******************************************************");
        System.out.println("Display all families after deleting all children older 10 years");
        System.out.println(familyController.displayAllFamilies());
        System.out.println("*******************************************************");

        corleone.addChild(child);
        corleone.addChild(child2);
        corleone.addChild(child3);
        corleone.addChild(child4);
        familyController.saveFamily(corleone);

        //A families of more than 5 members
        System.out.println("A families of more than 5 members");
        System.out.println(familyController.getFamiliesBiggerThan(4));

        //A families of less than 4 members
        System.out.println("A families of less than 4 members");
        System.out.println(familyController.getFamiliesLessThan(4));

        //A families equals 2 members
        System.out.println("A families equals 2 members");
        System.out.println(familyController.countFamiliesWithMemberNumber(2));

        //Get number of Families -> must be 3
        System.out.println(familyController.getNumberOfFamilies());

        //setPet and getPet

        System.out.println("Before adding pets");
        System.out.println("Pets in Corleone2 family");
        System.out.println(familyController.getPets(0));

        familyController.setPet(0, dog);
        familyController.setPet(0, fish);
        System.out.println("After adding pets");
        System.out.println("Pets in Corleone2 family");
        System.out.println(familyController.getPets(0));




    }
}
