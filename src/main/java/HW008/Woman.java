package HW008;

import java.util.HashMap;

public final class Woman extends Human {

    public Woman() {
    }

    public Woman(String name, String surname, int yearOfBirth) {
        super(name, surname, yearOfBirth);
    }

    public Woman(String name, String surname, int yearOfBirth, int iqLevel, Pet pet, Human mother, Human father, HashMap<String, String> schedule) {
        super(name, surname, yearOfBirth, iqLevel, pet, mother, father, schedule);
    }

    public void makeUp() {
        System.out.println("Я умею делать мейк-ап");
    }

    @Override
    public void greetPet() {
        if (super.getPet().isEmpty()) {
            System.out.println("У меня нет домашнего питомца :(");
        } else {
            for (Pet pet : super.getPet()) {
                System.out.printf("Привет, %s, рада тебя видеть", pet.getNickname());
            }
        }
    }
}
