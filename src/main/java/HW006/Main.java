package HW006;

import java.util.Arrays;

public class Main {
    public static void main(String[] args) {
        Pet dog = new Dog(Species.DOG, "Doggy", 10, 51, new String[]{"eat", "drink", "sleep"});
        Woman mother = new Woman("Jane", "Corleone", 1970);
        Man father = new Man("Vito", "Corleone", 1965);
        mother.setIqLevel(150);
        father.setIqLevel(140);
//        Human child = new Man("Antonio", "Corleone", 1995);
//        String[][] schedule = {{DayOfWeek.MONDAY.name(), "Got to work"}, {DayOfWeek.THURSDAY.name(), "Mee with John"}};
//
//        Human child2 = new Man("Mike", "Corleone", 1999);
//        Human child3 = new Human("Sarah", "Corleone", 1992);
//        Human child4 = new Man("Ben", "Corleone", 1997);
//        Human child5 = new Man("Luka", "Corleone", 1994);
//
//        System.out.println("==========================");
//        System.out.println(child);
//        System.out.println("==========================");
//
        Family corleone = new Family(mother, father);
//
        corleone.setPet(dog);
//        corleone.addChild(child);
//        corleone.addChild(child2);
//        corleone.addChild(child3);
//        corleone.addChild(child4);
//        corleone.addChild(child5);
//        System.out.println(corleone);
//        System.out.println("********************************************\n");
//        System.out.println(corleone.countFamily());
//        System.out.println(child.getFamily());
//        corleone.deleteChildByIndex(3);
//        corleone.deleteChild(child5);
//        System.out.println(corleone);
//        System.out.println(corleone.getChildrenInfo());
//        System.out.println(corleone.describePet());
//        System.out.println(child.getPet());
//        System.out.println("Family after child deleting: \n" + corleone.getChildrenInfo());
//        System.out.println(corleone.countFamily());
//
//        boolean petIsFed = child.feedPet(false);
//        System.out.println(petIsFed);
//        System.out.println(dog);
//        child.setSchedule(schedule);
//        System.out.println(child);
//        System.out.println("=============================");
//        System.out.println("Corleone family");
//        System.out.println(corleone);
//        System.out.println("=============================");
//        child.greetPet();
        Human newBorn = corleone.bornChild();
        System.out.println(newBorn);
        System.out.println(newBorn.getFamily());
        System.out.println(newBorn.getPet());
        System.out.println(Arrays.toString(corleone.getChildren()));


//        for (int i = 0; i < 10_000_000; i++) {
//            Human human = new Human();
//        }

    }
}
