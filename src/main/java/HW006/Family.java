package HW006;

import java.util.Arrays;
import java.util.Calendar;
import java.util.Objects;
import java.util.Random;

public class Family implements HumanCreator {
    private Woman mother;
    private Man father;
    private Human[] children;
    private Pet pet;

    private static final Random random = new Random();

    static {
        System.out.println("Class Family is loading...");
    }

    {
        children = new Human[0];
        System.out.printf("Class %s successfully created\n", this.getClass().getSimpleName());
    }

    public Family(Woman mother, Man father) {
        this.mother = mother;
        this.father = father;
    }

    //Getters and Setters
    public Human getMother() {
        return mother;
    }

    public void setMother(Woman mother) {
        this.mother = mother;
    }

    public Human getFather() {
        return father;
    }

    public void setFather(Man father) {
        this.father = father;
    }

    public Human[] getChildren() {
        return children;
    }

    public void setChildren(Human[] children) {
        this.children = children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
        setPetToChildren();
    }

    //Methods
    public void addChild(Human child) {
        children = Arrays.copyOf(children, children.length + 1);
        child.setFamily(this);
        children[children.length - 1] = child;
        setPetToChildren();
    }

    public void deleteChildByIndex(int index) {
        if (index > children.length - 1) {
            return;
        }
        children[index].setFamily(null);
        Human[] temp = new Human[children.length - 1];
        if (index == 0) {
            System.arraycopy(children, 1, temp, 0, temp.length);
        } else if (index == children.length - 1) {
            System.arraycopy(children, 0, temp, 0, temp.length);
        } else {
            System.arraycopy(children, 0, temp, 0, index);
            System.arraycopy(children, index + 1, temp, index, children.length - (index + 1));
        }

        children = temp;

    }

    public void deleteChild(Human childForDelete) {
        for (int i = 0; i < children.length; i++) {
            if (children[i].equals(childForDelete)) {
                deleteChildByIndex(i);
            }
        }
    }

    public int countFamily() {
        return children.length + 2;
    }

    public String parentsInfo() {
        return "Father: " + father +
                "\nMother: " + mother;
    }


    public String describePet() {
        String petTrick;
        if (pet == null) {
            return "The family has no pet";
        }
        if (pet.getTrickLevel() > 50) {
            petTrick = "очень хитрый";
        } else {
            petTrick = "почти не хитрый";
        }
        return "У нас есть " + pet.getSpecies() + ", ему " + pet.getAge() + " лет, он " + petTrick;
    }

    private void setPetToChildren() {

        if (children != null & pet != null) {
            for (Human child : children) {
                child.setPet(pet);
            }
        }
    }

    public String getChildrenInfo() {
        StringBuilder childrenInfo = new StringBuilder("Children:\n");
        for (Human child : children) {
            childrenInfo.append(child.getName()).append(" ").append(child.getSurname()).append(" ").append(child.getYearOfBirth()).append("\n");
        }
        return childrenInfo.toString();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return mother.equals(family.mother) && father.equals(family.father) && Arrays.equals(children, family.children);
    }

    @Override
    public int hashCode() {
        int result = Objects.hash(mother, father);
        result = 31 * result + Arrays.hashCode(children);
        return result;
    }

    @Override
    public String toString() {

        return "Family: " + father.getSurname() + "\nFather: " + father +
                "\nMother: " + mother +
                "\nChildren: " + Arrays.toString(children) +
                "\nPet: " + describePet();
    }

    @Override
    protected void finalize() {
        System.out.println("Объект " + this.getClass().getSimpleName() + " удален");
    }

   public Human bornChild() {
        int chance = random.nextInt(2);
        int iqLevel = (mother.getIqLevel() + father.getIqLevel()) / 2;
        int year = Calendar.getInstance().get(Calendar.YEAR);
        if (chance == 0) {
            int nameIndex = random.nextInt(MaleNames.values().length);
            Man newBornMan = new Man(MaleNames.values()[nameIndex].name(), father.getSurname(), year);
            newBornMan.setIqLevel(iqLevel);
            newBornMan.setFamily(this);
            newBornMan.setPet(pet);
            addChild(newBornMan);
            return newBornMan;

        } else {
            int nameIndex = random.nextInt(FemaleNames.values().length);
            Woman newBornWoman = new Woman(FemaleNames.values()[nameIndex].name(), father.getSurname(), year);
            newBornWoman.setIqLevel(iqLevel);
            newBornWoman.setFamily(this);
            newBornWoman.setPet(pet);
            addChild(newBornWoman);
            return newBornWoman;
        }
    }
}


