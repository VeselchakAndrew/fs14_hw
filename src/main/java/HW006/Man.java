package HW006;

public final class Man extends Human {
    public Man() {
    }

    public Man(String name, String surname, int yearOfBirth) {
        super(name, surname, yearOfBirth);
    }

    public Man(String name, String surname, int yearOfBirth, int iqLevel, Pet pet, Human mother, Human father, String[][] schedule) {
        super(name, surname, yearOfBirth, iqLevel, pet, mother, father, schedule);
    }

    public void repairCar() {
        System.out.println("Я умею чинить автомобиль");
    }

    @Override
    public void greetPet() {
        System.out.printf("Привет, %s", super.getPet().getNickname());
    }
}
