package HW006;

import java.util.Arrays;
import java.util.Objects;
import java.util.Random;

public class Human {
    private String name;
    private String surname;
    private int yearOfBirth;
    private int iqLevel;
    private String[][] schedule;
    private Family family;
    private Pet pet;
    private Human father;
    private Human mother;

    private static final Random random = new Random();

    static {
        System.out.println("Class Human is loading...");
    }

    {
        System.out.printf("Class %s successfully created\n", this.getClass().getSimpleName());
    }

    public Human() {
    }

    public Human(String name, String surname, int yearOfBirth) {
        this.name = name;
        this.surname = surname;
        this.yearOfBirth = yearOfBirth;
    }

    public Human(String name, String surname, int yearOfBirth, int iqLevel, Pet pet, Human mother, Human father, String[][] schedule) {
        this.name = name;
        this.surname = surname;
        this.yearOfBirth = yearOfBirth;
        this.iqLevel = iqLevel;
        this.schedule = schedule;
    }

    //Getters and Setters
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYearOfBirth() {
        return yearOfBirth;
    }

    public void setYearOfBirth(int yearOfBirth) {
        this.yearOfBirth = yearOfBirth;
    }

    public int getIqLevel() {
        return iqLevel;
    }

    public void setIqLevel(int iqLevel) {
        this.iqLevel = iqLevel;
    }

    public String[][] getSchedule() {
        return schedule;
    }

    public void setSchedule(String[][] schedule) {
        this.schedule = schedule;
    }

    public String getFamily() {
        return family.parentsInfo();
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }

    //Methods
    public boolean feedPet(boolean timeToFeed) {
        boolean checkForTrick = false;
        if (!timeToFeed) {
            if (pet.getTrickLevel() > random.nextInt(100)) {
                checkForTrick = true;
            }

            if (checkForTrick | timeToFeed) {
                System.out.printf("Хм... покормлю ка я %s\n", pet.getNickname());
                return true;
            } else {
                System.out.printf("Думаю, %s не голоден.\n", pet.getNickname());
                return false;
            }
        }
        System.out.printf("Хм... покормлю ка я %s\n", pet.getNickname());
        return true;
    }

    public void greetPet() {
        System.out.printf("Привет, %s\n", pet.getNickname());
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return yearOfBirth == human.yearOfBirth && iqLevel == human.iqLevel && Objects.equals(name, human.name) && Objects.equals(surname, human.surname);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, yearOfBirth, iqLevel);
    }

    @Override
    public String toString() {
        String className = this.getClass().getSimpleName();
        return className + " {name = " + name + ", surname = " + surname + ", year = " + yearOfBirth + ", iq = " + iqLevel
                + ", schedule = " + Arrays.deepToString(getSchedule()) + "}";
    }

    @Override
    protected void finalize() {
        System.out.println("Объект " + this.getClass().getSimpleName() + " удален");
    }
}