package HW006;

public class Fish extends Pet{

    private Species species = Species.FISH;

    public Fish(Species species, String nickname, int age, int trickLevel, String[] habits) {
        super(species, nickname, age, trickLevel, habits);
    }

    public Fish(Species species, String nickname) {
        super(species, nickname);
    }

    public Fish() {

    }
    @Override
    public void respond() {
        System.out.printf("Привет! Я %s. Покорми меня.", super.getNickname());
    }

    @Override
    public Species getSpecies() {
        return this.species;
    }

    @Override
    public void setSpecies(Species species) {
        this.species = species;
    }
}
