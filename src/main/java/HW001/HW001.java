package HW001;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class HW001 {

    private static final Scanner scanner = new Scanner(System.in);
    private static final Random random = new Random();

    public static void main(String[] args) {

        System.out.print("Enter your name: ");
        String userName = scanner.nextLine();
        System.out.println("Hi, " + userName + "!");
        System.out.println("Let the game begin!");
        boolean isRunning = true;
        int guessedNumber = random.nextInt(100);
        int attempts = 0;
        int[] answers = new int[5];

        while (isRunning) {
            System.out.printf("Attempt #: %d\n", attempts + 1);
            System.out.println("Enter your number: ");
            String userInput = scanner.nextLine();
            try {
                int numberToGuess = Integer.parseInt(userInput);
                if (answers.length <= attempts) {
                    int[] temp = new int[answers.length + 5];
                    System.arraycopy(answers, 0, temp, 0, answers.length);
                    answers = temp;
                }
                answers[attempts] = numberToGuess;
                attempts++;
                isRunning = gameProcess(numberToGuess, guessedNumber, userName, attempts, answers);
            } catch (NumberFormatException e) {
                System.err.print("Enter correct digit!\n");
            }
        }
    }

    private static int[] reverseSortedArr(int[] arr) {
        Arrays.sort(arr);
        int[] reversedArr = new int[arr.length];
        for (int i = 0; i < arr.length; i++) {
            reversedArr[i] = arr[arr.length - 1 - i];
        }
        return reversedArr;
    }

    private static boolean gameProcess(int numberToGuess, int guessedNumber, String userName, int attempts, int[] answers) {
        boolean isRunning = true;
        if (numberToGuess == guessedNumber) {
            int[] finalAttemptsNumber = Arrays.copyOf(answers, attempts);
            finalAttemptsNumber = reverseSortedArr(finalAttemptsNumber);
            System.out.printf("Congratulations, %s!\n", userName);
            System.out.printf("Your guessed in %d attempts\n", attempts);
            System.out.println("Your answers: " + Arrays.toString(finalAttemptsNumber));
            isRunning = false;
        } else if (numberToGuess > guessedNumber) {
            System.out.println("Your number is too big. Please, try again.");
        } else {
            System.out.println("Your number is too small. Please, try again.");
        }
        return isRunning;
    }
}
