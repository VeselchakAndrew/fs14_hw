package HW007;

import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;

public class Main {
    public static void main(String[] args) {
        Pet dog = new Dog(Species.DOG, "Doggy", 10, 51, new HashSet<>(Arrays.asList("eat", "drink", "sleep")));
        Pet fish = new Fish(Species.FISH, "Goldy", 1, 10, new HashSet<>(Arrays.asList("eat", "swim", "sleep")));
        Pet cat = new DomesticCat(Species.DOMESTIC_CAT, "Kitty", 3, 99, new HashSet<>(Arrays.asList("eat", "sleep", "foul")));
        Woman mother = new Woman("Jane", "Corleone", 1970);
        Man father = new Man("Vito", "Corleone", 1965);
        mother.setIqLevel(150);
        father.setIqLevel(140);


        Human child = new Man("Antonio", "Corleone", 1995);
//        Map<String, String> schedule = new HashMap<>();
//        schedule.put(DayOfWeek.MONDAY.name(), "Go to work");
//        schedule.put(DayOfWeek.THURSDAY.name(), "Meet with John");
////
//        Human child2 = new Man("Mike", "Corleone", 1999);
//        Human child3 = new Human("Sarah", "Corleone", 1992);
//        Human child4 = new Man("Ben", "Corleone", 1997);
//        Human child5 = new Man("Luka", "Corleone", 1994);
////
////        System.out.println("==========================");
////        System.out.println(child);
////        System.out.println("==========================");
////
//        Family corleone = new Family(mother, father);
////
//        corleone.addPet(dog);
//        corleone.addChild(child);
//        corleone.addChild(child2);
//        corleone.addChild(child3);
//        corleone.addChild(child4);
//        corleone.addChild(child5);
//        System.out.println(corleone);
//        System.out.println("********************************************\n");
//        System.out.println(corleone.countFamily());
//        System.out.println(child.getFamily());
//        corleone.deleteChildByIndex(3);
//        corleone.deleteChild(child5);
//        System.out.println(corleone);
//        System.out.println(corleone.getChildrenInfo());
//        System.out.println(corleone.describePet());
//        System.out.println(child.getPet());
//        System.out.println("Family after child deleting: \n" + corleone.getChildrenInfo());
//        System.out.println(corleone.countFamily());
//
//        boolean petIsFed = child.feedPet(false, dog);
//        System.out.println(petIsFed);
//        System.out.println(dog);
//        child.setSchedule(schedule);
////        System.out.println(child);
//        System.out.println("=============================");
//        System.out.println("Corleone family");
//        System.out.println(corleone);
//        System.out.println("=============================");
//        child.greetPet();
//        System.out.println("=============================");
//        Human newBorn = corleone.bornChild();
//        System.out.println(newBorn);
//        System.out.println(newBorn.getFamily());
//        System.out.println(newBorn.getPet());
//        System.out.println(corleone.getChildren());
//
//        child.feedPet(true, fish);
//        corleone.addPet(cat);
//        child.greetPet();
//        System.out.println(corleone.getPet());
//        System.out.println(child.getPet());
//        child.feedPet(false, cat);


    }
}
