package HW007;

import java.util.Set;

public class DomesticCat extends Pet implements PetFoul {

    private Species species = Species.DOMESTIC_CAT;

    public DomesticCat(Species species, String nickname, int age, int trickLevel, Set<String> habits) {
        super(species, nickname, age, trickLevel, habits);
    }

    public DomesticCat(Species species, String nickname) {
        super(species, nickname);
    }

    public DomesticCat() {

    }

    @Override
    public void respond() {
        System.out.printf("Meow, хозяин. Я - %s. Я соскучился!\n", super.getNickname());
    }

    @Override
    public void makeFoul() {
        System.out.println("Meow! Нужно хорошо замести следы...");
    }

    @Override
    public Species getSpecies() {
        return this.species;
    }

    @Override
    public void setSpecies(Species species) {
        this.species = species;
    }
}
