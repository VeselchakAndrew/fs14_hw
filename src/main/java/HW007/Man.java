package HW007;

import java.util.HashMap;

public final class Man extends Human {
    public Man() {
    }

    public Man(String name, String surname, int yearOfBirth) {
        super(name, surname, yearOfBirth);
    }

    public Man(String name, String surname, int yearOfBirth, int iqLevel, Pet pet, Human mother, Human father, HashMap<String, String> schedule) {
        super(name, surname, yearOfBirth, iqLevel, pet, mother, father, schedule);
    }

    public void repairCar() {
        System.out.println("Я умею чинить автомобиль");
    }

    @Override
    public void greetPet() {
        if (super.getPet().isEmpty()) {
            System.out.println("У меня нет домашнего питомца :(");
        } else {
            for (Pet pet : super.getPet()) {
                System.out.printf("Привет, %s\n", pet.getNickname());
            }
        }
    }
}

