package HW012;

import java.text.ParseException;

public interface HumanCreator {
    Human bornChild() throws ParseException;
}
