package HW012;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDao implements FamilyDAO {

    private List<Family> families = new ArrayList<>();

    public List<Family> getAllFamilies() {
        return families;
    }


    public Family getFamilyByIndex(int index) {
        if (index >= families.size() || index < 0) {
            return null;
        } else {
            return families.get(index);
        }
    }


    public Boolean deleteFamily(int index) {
        if (index >= families.size() || index < 0) {
            return false;
        } else {
            families.remove(index);
            return true;
        }
    }

    public Boolean deleteFamily(Family family) {
        if (!families.contains(family)) {
            return false;
        } else {
            deleteFamily(families.indexOf(family));
            return true;
        }
    }

    public void deleteAllFamilies() {
        families.clear();
    }

    public void saveFamily(Family family) {
        if (families.contains(family)) {
            families.set(families.indexOf(family), family);
        } else {
            families.add(family);
        }
    }

    public void loadData(File file) throws IOException, ClassNotFoundException {
        ObjectInputStream stream = new ObjectInputStream(new FileInputStream(file));
        families = (List<Family>) stream.readObject();
        stream.close();
    }

    public void saveData(File file, List<Family> families) throws IOException {
        ObjectOutputStream stream = new ObjectOutputStream(new FileOutputStream(file));
        stream.writeObject(families);
        stream.close();
    }
}
