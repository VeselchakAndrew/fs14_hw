package HW012;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;

public interface FamilyDAO {
    List<Family> getAllFamilies();

    Family getFamilyByIndex(int index);

    Boolean deleteFamily(int index);

    Boolean deleteFamily(Family family);

    void saveFamily(Family family);

    void loadData(File file) throws IOException, ClassNotFoundException;

    void saveData(File file, List<Family> families) throws IOException;

}