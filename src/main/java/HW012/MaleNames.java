package HW012;

public enum MaleNames {
    JOHN,
    BOB,
    OLIVER,
    JACK,
    HARRY,
    JACOB,
    CHARLEY,
    THOMAS,
    GEORGE,
    OSCAR;
}
