package HW012;

import java.io.Serializable;
import java.text.ParseException;
import java.util.HashMap;

public final class Man extends Human implements Serializable {
    public Man() {
    }

    public Man(String name, String surname, String birthDate) throws ParseException {
        super(name, surname, birthDate);
    }

    public Man(String name, String surname, String birthDate, int iqLevel) throws ParseException {
        super(name, surname, birthDate, iqLevel);
    }

    public Man(String name, String surname, String birthDate, int iqLevel, Pet pet, Human mother, Human father, HashMap<String, String> schedule) throws ParseException {
        super(name, surname, birthDate, iqLevel, pet, mother, father, schedule);
    }

    public void repairCar() {
        System.out.println("Я умею чинить автомобиль");
    }

    @Override
    public void greetPet() {
        if (super.getPet().isEmpty()) {
            System.out.println("У меня нет домашнего питомца :(");
        } else {
            for (Pet pet : super.getPet()) {
                System.out.printf("Привет, %s\n", pet.getNickname());
            }
        }
    }
}

