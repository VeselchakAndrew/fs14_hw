package HW012;

import java.io.Serializable;
import java.util.Set;

public class RoboCat extends Pet implements Serializable {

    private Species species = Species.ROBOCAT;

    public RoboCat(Species species, String nickname, int age, int trickLevel, Set<String> habits) {
        super(species, nickname, age, trickLevel, habits);
    }

    public RoboCat(Species species, String nickname) {
        super(species, nickname);
    }

    public RoboCat() {

    }

    @Override
    public void respond() {
        System.out.printf("Я робокот. Меня зовут %s. Я соскучился!\n", super.getNickname());
    }

    @Override
    public void eat() {
        System.out.println("Подключите меня к розетке.");
    }

    @Override
    public Species getSpecies() {
        return this.species;
    }

    @Override
    public void setSpecies(Species species) {
        this.species = species;
    }
}
