package HW012;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.util.List;
import java.util.Set;

public class FamilyController {
    private final FamilyService familyService = new FamilyService();

    public List<Family> getAllFamilies() {
        return familyService.getAllFamilies();
    }

    public void displayAllFamilies() {
        familyService.displayAllFamilies();
    }

    public void saveFamily(Family family) {
        familyService.saveFamily(family);
    }

    public List<Family> getFamiliesBiggerThan(int numberOfFamilyMembers) {
        return familyService.getFamiliesBiggerThan(numberOfFamilyMembers);
    }

    public List<Family> getFamiliesLessThan(int numberOfFamilyMembers) {
        return familyService.getFamiliesLessThan(numberOfFamilyMembers);
    }

    public List<Family> countFamiliesWithMemberNumber(int numberOfFamilyMembers) {
        return familyService.countFamiliesWithMemberNumber(numberOfFamilyMembers);
    }

    public void createNewFamily(Man man, Woman woman) {
        familyService.createNewFamily(man, woman);
    }

    public Boolean deleteFamilyByIndex(int index) {
        return familyService.deleteFamilyByIndex(index);
    }

    public Boolean deleteFamily(Family family) {
        return familyService.deleteFamily(family);
    }

    public void deleteAllFamilies() {
        familyService.deleteAllFamilies();
    }

    public void bornChild(Family family) throws ParseException {
        familyService.bornChild(family);
    }

    public void adoptChild(Family family, Human child) {
        familyService.adoptChild(family, child);
    }

    public void deleteAllChildrenOlderThen(int age) throws ParseException {
        familyService.deleteAllChildrenOlderThen(age);
    }

    public int getNumberOfFamilies() {
        return familyService.getNumberOfFamilies();
    }

    public Family getFamilyById(int index) {
        return familyService.getFamilyById(index);
    }

    public Set<Pet> getPets(int index) {
        return familyService.getPets(index);
    }

    public void setPet(int index, Pet pet) {
        familyService.setPet(index, pet);
    }

    public void makeTestFamilies(int numberOfFamilies) throws ParseException {
        familyService.makeTestFamilies(numberOfFamilies);
    }

    public void loadData(File file) throws IOException, ClassNotFoundException {
        familyService.loadData(file);
    }

    public void saveData(File file, List<Family> families) throws IOException {
        familyService.saveData(file, families);
    }
}
